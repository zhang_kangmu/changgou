package com.changgou.item.service;

/**
 * Created by zhangyuhong
 * Date:2020/5/16
 */
public interface PageService {
    /**
     * 根据商品的ID 生成静态页
     * @param spuId
     */
    public void createPageHtml(Long spuId) ;
}