package com.changgou;

import org.aspectj.weaver.ast.Var;
import org.csource.common.MyException;
import org.csource.fastdfs.*;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Created by zhangyuhong
 * Date:2020/5/6
 */
public class FastdfsClientTest {
    @Test
    public void upload() throws Exception {
        //1.创建一个配置文件用于链接tracker服务器

        //2.加载配置文件
        ClientGlobal.init("H:\\javaProject\\project\\changgou\\changgouparent\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");
        //3.创建trackerclient 对象
        TrackerClient trackerClient = new TrackerClient();
        //4.创建trackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        //5.创建storageServer对象
        StorageServer storageServer = null;
        //6.创建storageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, storageServer);
        //7.storageClient有许多的方法 比如上传图片
        //参数1 指定要上传的图片的路径
        //参数2 指定图片的扩展名 注意不要带点
        //参数3 指定的图片的元数据 比如图片的像素 高度 拍摄日期 作者，大小
        String[] jpgs = storageClient.upload_file("C:\\Users\\28959\\Pictures\\测试照片\\01.png", "jpg", null);
        for (String jpg : jpgs) {
            //group1
            //M00/00/00/wKjThF6y1eaAQqubAAECGg-Cl4k698.jpg
            System.out.println(jpg);
        }
    }

    //图片下载
    @Test
    public void download() throws Exception {
        //1.创建一个配置文件用于链接tracker服务器

        //2.加载配置文件
        ClientGlobal.init("H:\\javaProject\\project\\changgou\\changgouparent\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");
        //3.创建trackerclient 对象
        TrackerClient trackerClient = new TrackerClient();
        //4.创建trackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        //5.创建storageServer对象
        StorageServer storageServer = null;
        //6.创建storageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, storageServer);
        //7.storageClient有许多的方法 比如上传图片
        //参数1 指定组名
        //参数2 指定要下的图片的路径
        byte[] bytes = storageClient.download_file("group1", "M00/00/00/wKjThF6y1eaAQqubAAECGg-Cl4k698.jpg");
        FileOutputStream fileOutputStream = new FileOutputStream(new File("E:\\123123.png"));
        fileOutputStream.write(bytes);
        fileOutputStream.close();
    }

    //图片的删除
    @Test
    public void delete() throws Exception {
        //1.创建一个配置文件用于链接tracker服务器

        //2.加载配置文件
        ClientGlobal.init("H:\\javaProject\\project\\changgou\\changgouparent\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");

        //3.创建trackerclient 对象
        TrackerClient trackerClient = new TrackerClient();

        //4.创建trackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();

        //5.创建storageServer对象
        StorageServer storageServer = null;

        //6.创建storageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, storageServer);
        int group1 = storageClient.delete_file("group1", "M00/00/00/wKjThF6y1eaAQqubAAECGg-Cl4k698.jpg");
        if (group1 == 0) {

            System.out.println("success");
        } else {
            System.out.println("failed");
        }
    }

    //获取文件的信息数据
    @Test
    public void getFileInfo() throws Exception {
        //1.创建一个配置文件用于链接tracker服务器
        //2.加载配置文件
        ClientGlobal.init("H:\\javaProject\\project\\changgou\\changgouparent\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");
        //3.创建trackerclient 对象
        TrackerClient trackerClient = new TrackerClient();
        //4.创建trackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        //5.创建storageServer对象
        StorageServer storageServer = null;
        //6.创建storageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, storageServer);
        FileInfo group1 = storageClient.get_file_info("group1", "M00/00/00/wKjThF6y5ZqAer0iAAECGg-Cl4k572.jpg");
        System.out.println(group1);//storage的服务ip,文件大小,创建日期
        //source_ip_addr = 192.168.211.132, file_size = 66074, create_timestamp = 2020-05-07 00:28:10, crc32 = 260216713
    }

    //获取组相关的信息
    @Test
    public void getGroupInfo() throws Exception {
        //1.创建一个配置文件用于链接tracker服务器
        //2.加载配置文件
        ClientGlobal.init("H:\\javaProject\\project\\changgou\\changgouparent\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");
        //3.创建trackerclient 对象
        TrackerClient trackerClient = new TrackerClient();
        //4.创建trackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        StorageServer group1 = trackerClient.getStoreStorage(trackerServer, "group1");
        System.out.println(group1);  //org.csource.fastdfs.StorageServer@6debcae2  返回的是一个地址

        //组对应的服务器的地址  因为有可能有多个服务器.
        ServerInfo[] group1s = trackerClient.getFetchStorages(trackerServer, "group1", "M00/00/00/wKjThF6y5ZqAer0iAAECGg-Cl4k572.jpg");
        for (ServerInfo serverInfo : group1s) {
            System.out.println(serverInfo.getIpAddr());
            System.out.println(serverInfo.getPort());
        }
    }

    @Test
    public void getTrackerInfo() throws Exception {
        //加载全局的配置文件
        ClientGlobal.init("H:\\javaProject\\project\\changgou\\changgouparent\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");

        //创建TrackerClient客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象获取TrackerServer信息
        TrackerServer trackerServer = trackerClient.getConnection();

        //fdfs_client.conf里的tracker的http端口
        int g_tracker_http_port = ClientGlobal.getG_tracker_http_port();
        System.out.println(g_tracker_http_port);
        //tracker服务器IP和端口
        InetSocketAddress inetSocketAddress = trackerServer.getInetSocketAddress();
        System.out.println(inetSocketAddress);

    }
}
