package com.changgou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.TestDatabaseAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Created by zhangyuhong
 * Date:2020/5/6
 */
//排除掉数据源的自动配置类 不需要自动配置，不需要连接数据库
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient  //启用eureka的client
public class FileApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class,args);
    }
}
