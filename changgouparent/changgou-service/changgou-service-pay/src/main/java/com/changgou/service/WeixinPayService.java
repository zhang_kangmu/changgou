package com.changgou.service;

import java.util.Map;

/**
 * Created by zhangyuhong
 * Date:2020/5/20
 */
public interface WeixinPayService {
    /***
     * 创建二维码 的接口
     * @return
     */
    public Map createNative(Map<String,String> parameter);

    Map<String, String> queryPayStatus(String out_trade_no);

    /***
     * 关闭支付
     * @param orderId
     * @return
     */
    Map<String,String> closePay(Long orderId) throws Exception;
}
