package com.changgou.search.dao;

import com.changgou.search.pojo.SkuInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Created by zhangyuhong
 * Date:2020/5/12
 */
public interface SkuEsMapper extends ElasticsearchRepository<SkuInfo,Long> {
}
