package com.changgou.search.service;

import java.util.Map;

/**
 * Created by zhangyuhong
 * Date:2020/5/12
 */
public interface SkuService {

    /***
     * 导入SKU数据
     */
    void importSku();
    /***
     * 搜索
     * @param searchMap
     * @return
     */
    Map search(Map<String, String> searchMap);
}
