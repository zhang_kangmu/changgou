package com.changgou.search.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.search.service.SkuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by zhangyuhong
 * Date:2020/5/12
 */
@Api("搜索相关api")
@RestController
@RequestMapping(value = "/search")
@CrossOrigin
public class SkuController {

    @Autowired
    private SkuService skuService;

    /**
     * 导入数据 到elasticsearch
     * @return
     */
    @ApiOperation("将商品详情导入到es中")
    @GetMapping("/import")
    public Result search(){
        skuService.importSku();
        return new Result(true, StatusCode.OK,"导入数据到索引库中成功！");
    }

    /**
     * 搜索
     * @param searchMap
     * @return
     */
    @ApiOperation("关键字查询商品")
//    @PostMapping
    @GetMapping
//    public Map search(@RequestBody(required = false) Map searchMap){
    public Map search(@RequestParam(required = false) Map searchMap){
        return  skuService.search(searchMap);
    }
}
