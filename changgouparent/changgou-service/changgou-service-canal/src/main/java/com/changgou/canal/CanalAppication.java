package com.changgou.canal;

import com.xpand.starter.canal.annotation.EnableCanalClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Created by zhangyuhong
 * Date:2020/5/11
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableCanalClient  //开启canal客户端
@EnableEurekaClient  //属于eureka客户端
@EnableFeignClients(basePackages = {"com.changgou.content.feign","com.changgou.item.feign"})  //开启feign,并指定位置
public class CanalAppication {
    public static void main(String[] args) {
        SpringApplication.run(CanalAppication.class,args);
    }
}
