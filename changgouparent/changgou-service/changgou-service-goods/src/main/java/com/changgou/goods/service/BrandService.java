package com.changgou.goods.service;

import com.changgou.goods.pojo.Brand;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * Created by zhangyuhong
 * Date:2020/5/5
 */
public interface BrandService {
    List<Brand> findAll();

    Brand findById(Integer id);

    void update(Brand brand);

    void add(Brand brand);

    void delete(Integer id);

    List<Brand> findList(Brand brand);

    PageInfo<Brand> getPage(Integer page, Integer pageSize);

    PageInfo<Brand> findPage(Brand brand, Integer page, Integer pageSize);

    List<Brand> findBrandByCategory(Integer id);
}
