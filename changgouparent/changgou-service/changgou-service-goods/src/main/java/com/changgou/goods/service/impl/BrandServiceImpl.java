package com.changgou.goods.service.impl;

import com.changgou.goods.dao.BrandMapper;
import com.changgou.goods.dao.CategoryMapper;
import com.changgou.goods.pojo.Brand;
import com.changgou.goods.service.BrandService;
import com.changgou.goods.service.CategoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * Created by zhangyuhong
 * Date:2020/5/5
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    BrandMapper brandMapper;
    @Autowired
    CategoryMapper categoryMapper;

    @Override
    public List<Brand> findAll() {
        return brandMapper.selectAll();
    }

    @Override
    public Brand findById(Integer id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    @Override
    public void update(Brand brand) {
        brandMapper.updateByPrimaryKeySelective(brand);
    }

    @Override
    public void add(Brand brand) {
        brandMapper.insertSelective(brand);
    }

    @Override
    public void delete(Integer id) {
        brandMapper.deleteByPrimaryKey(id);
    }

    @Override
    public List<Brand> findList(Brand brand) {
        Example example = createExample(brand);
        List<Brand> list = brandMapper.selectByExample(example);
        return list;
    }

    @Override
    public PageInfo<Brand> getPage(Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        List<Brand> list = brandMapper.selectAll();
        PageInfo<Brand> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public PageInfo<Brand> findPage(Brand brand, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        Example example = createExample(brand);
        List<Brand> list = brandMapper.selectByExample(example);
        PageInfo<Brand> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * @param brand  查询的条件
     * @return  根据查询条件返回条件对象  Example里封装了条件
     */
    private Example createExample(Brand brand) {
        Example example = new Example(Brand.class);
        Example.Criteria criteria = example.createCriteria();//连接条件类
        if (brand != null) {
            if (StringUtils.isNotEmpty(brand.getName())) {
                criteria.andLike("name","%"+brand.getName()+"%");  //模糊查询不要漏了百分号了
            }
            if (StringUtils.isNotEmpty(brand.getImage())) {
                criteria.andLike("image","%"+brand.getImage()+"%");
            }
            if (StringUtils.isNotEmpty(brand.getLetter())) {
                criteria.andLike("letter","%"+brand.getLetter()+"%");
            }
            if (brand.getId() != null) {
                criteria.andEqualTo("id",brand.getId());
            }
            if (StringUtils.isNotEmpty(brand.getSeq()+"")) {  //这里是字符串  有问题,问老师,笔记上写的是直接判断字符串
                criteria.andEqualTo("seq",brand.getSeq());
            }
        }
        return example;
    }
    @Override
    public List<Brand> findBrandByCategory(Integer id) {
        // select tbb.* from tb_category_brand tcb,tb_brand tbb where tcb.category_id=76  and tcb.brand_id=tbb.id
        return brandMapper.findBrandByCategory(id);
    }
}
