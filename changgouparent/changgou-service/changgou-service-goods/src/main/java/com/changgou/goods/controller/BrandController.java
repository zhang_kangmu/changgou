package com.changgou.goods.controller;

import com.changgou.goods.pojo.Brand;
import com.changgou.goods.service.BrandService;
import com.github.pagehelper.PageInfo;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by zhangyuhong
 * Date:2020/5/5
 */
@Api(description = "品牌API")
@RestController
@RequestMapping("/brand")
@CrossOrigin
public class BrandController {
    @Autowired
    BrandService brandService;

    //查询所有的品牌的列表
    //@RequestMapping(value="/findAll",method = RequestMethod.GET)=  @GetMapping("/findAll")
    @GetMapping
    public Result<List<Brand>> findAll() {
        List<Brand> bandList = brandService.findAll();
//        int i=1/0;  //测试统一异常处理类
        return new Result<List<Brand>>(true, StatusCode.OK, "查询品牌列表成功", bandList);
    }

    /**
     * 根据品牌的ID 获取品牌的数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Brand> findById(@PathVariable(name = "id") Integer id) {
        Brand brand = brandService.findById(id);
        return new Result<Brand>(true, StatusCode.OK, "根据ID查询品牌成功", brand);
    }

    /**
     * 更新数据
     *
     * @param brand 用于接收页面传递过来的表单的数据JSON
     * @param id    传递过来的品牌的iD主键值
     * @return
     */
    @PutMapping("/{id}")
    public Result update(@RequestBody Brand brand, @PathVariable(name = "id") Integer id) {
        brand.setId(id);
        brandService.update(brand);
        return new Result(true, StatusCode.OK, "更新成功");
    }

    /**
     * 增加品牌
     *
     * @param brand 增加的品牌信息
     * @return
     */
    @PutMapping
    public Result addBrand(@RequestBody Brand brand) {
        brandService.add(brand);
        return new Result(true, StatusCode.OK, "增加品牌列表成功");
    }

    /**
     * 根据id删除品牌
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deletedById(@PathVariable Integer id) {
        brandService.delete(id);
        return new Result(true, StatusCode.OK, "删除品牌列表成功");
    }

    /***
     * 多条件搜索品牌数据
     * @param brand
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<Brand>> findList(@RequestBody(required = false) Brand brand) {
        List<Brand> list = brandService.findList(brand);
        return new Result<List<Brand>>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 分页查询品牌
     *
     * @param page     当前页码
     * @param pageSize 页面记录数量
     * @return
     */
    @GetMapping("/{page}/{pageSize}")
    public Result getPage(@PathVariable Integer page, @PathVariable Integer pageSize) {
        PageInfo<Brand> list = brandService.getPage(page, pageSize);
        return new Result<List<Brand>>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 分页查询品牌
     *
     * @param page     当前页码
     * @param pageSize 页面记录数量
     * @return
     */
    @GetMapping("/search/{page}/{pageSize}")
    public Result findPage(
            @RequestBody(required = false) Brand brand, //默认是等于true的
            @PathVariable Integer page, @PathVariable Integer pageSize) {
        PageInfo<Brand> list = brandService.findPage(brand, page, pageSize);
        return new Result<List<Brand>>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 查询商品分类的关联到的品牌列表
     *
     * @param id 点击到的商品分类的id
     * @return
     */
    @ApiOperation(value = "根据商品分类查询品牌")
    @GetMapping("/category/{id}")
    public Result<List<Brand>> findBrandByCategory(
            @ApiParam(name = "id",value = "分类id")
            @PathVariable(name = "id") Integer id) {
        List<Brand> brandList = brandService.findBrandByCategory(id);
        return new Result<List<Brand>>(true, StatusCode.OK,"查询品牌成功",brandList) ;
    }
}
