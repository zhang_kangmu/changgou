package com.changgou.goods.dao;

import com.changgou.goods.pojo.Brand;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * Created by zhangyuhong
 * Date:2020/5/5
 */
public interface BrandMapper extends Mapper<Brand> {
@Select("SELECT tbb.`name` FROM tb_brand tbb LEFT JOIN tb_category_brand tbcb ON tbcb.`category_id`=#{id} WHERE tbcb.`brand_id`=tbb.`id`")
    List<Brand> findBrandByCategory(Integer id);
}
