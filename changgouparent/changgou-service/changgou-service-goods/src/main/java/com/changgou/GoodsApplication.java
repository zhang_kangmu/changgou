package com.changgou;

import com.changgou.entity.IdWorker;
import com.alibaba.druid.pool.DruidDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;


/**
 * Created by zhangyuhong
 * Date:2020/5/5
 */
@SpringBootApplication
@EnableEurekaClient//启用eureka client
@MapperScan("com.changgou.goods.dao")//使用通用mapper提供的组件扫描
//@ComponentScan(basePackages={"com.changgou"})  //如果不指定扫描路径，那么就扫描该注解修饰的启动类所在的包以及子包,所以让异常类放在com.changgou下就不用扫描了
public class GoodsApplication {
    public static void main(String[] args) {
        SpringApplication.run(GoodsApplication.class, args);
    }
    @Bean(name = "idWorker")
    public IdWorker getIdWorker(){
       return new IdWorker();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        return druidDataSource;
    }

    @Primary
    @Bean("dataSourceProxy")
    public DataSourceProxy dataSourceProxy(DataSource dataSource) {
        return new DataSourceProxy(dataSource);
    }
}
