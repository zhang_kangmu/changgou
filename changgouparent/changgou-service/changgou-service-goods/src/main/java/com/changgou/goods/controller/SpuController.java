package com.changgou.goods.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.goods.pojo.Goods;
import com.changgou.goods.pojo.Spu;
import com.changgou.goods.service.SpuService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/****
 * @Author:admin
 * @Description:
 * @Date 2019/6/14 0:18
 *****/
@Api(description = "商品基本信息API")
@RestController
@RequestMapping("/spu")
@CrossOrigin
public class SpuController {

    @Autowired
    private SpuService spuService;

    /***
     * Spu分页条件搜索实现
     * @param spu
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "分页条件查询商品列表")
    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo> findPage(@RequestBody(required = false) Spu spu, @PathVariable int page, @PathVariable int size) {
        //调用SpuService实现分页条件查询Spu
        PageInfo<Spu> pageInfo = spuService.findPage(spu, page, size);
        return new Result(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /***
     * Spu分页搜索实现
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo> findPage(@PathVariable int page, @PathVariable int size) {
        //调用SpuService实现分页查询Spu
        PageInfo<Spu> pageInfo = spuService.findPage(page, size);
        return new Result<PageInfo>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /***
     * 多条件搜索品牌数据
     * @param spu
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<Spu>> findList(@RequestBody(required = false) Spu spu) {
        //调用SpuService实现条件查询Spu
        List<Spu> list = spuService.findList(spu);
        return new Result<List<Spu>>(true, StatusCode.OK, "查询成功", list);
    }

    /***
     * 根据ID删除品牌数据
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result delete(@PathVariable Long id) {
        //调用SpuService实现根据主键删除
        spuService.delete(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /***
     * 修改Spu数据
     * @param spu
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result update(@RequestBody Spu spu, @PathVariable Long id) {
        //设置主键值
        spu.setId(id);
        //调用SpuService实现修改Spu
        spuService.update(spu);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /***
     * 新增Spu数据
     * @param spu
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Spu spu) {
        //调用SpuService实现添加Spu
        spuService.add(spu);
        return new Result(true, StatusCode.OK, "添加成功");
    }

    /***
     * 根据ID查询Spu数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Spu> findById(@PathVariable(name = "id") Long id) {
        //调用SpuService实现根据主键查询Spu
        Spu spu = spuService.findById(id);
        return new Result<Spu>(true, StatusCode.OK, "查询成功", spu);
    }

    /***
     * 查询Spu全部数据SpuFeign
     * @return
     */
    @GetMapping
    public Result<List<Spu>> findAll() {
        //调用SpuService实现查询所有Spu
        List<Spu> list = spuService.findAll();
        return new Result<List<Spu>>(true, StatusCode.OK, "查询成功", list);
    }
/* 测试参数
{
	"skuList": [{
"sn":"f924c40c1b924d9588dd702c4b3955s2",
			"num": 100,
			"price": 222,
			"saleNum": 2,
			"spec": "{\"电视音响效果\":\"测试立体声\",\"测试电视屏幕尺寸\":\"测试20英寸\",\"尺码\":\"测试165\"}",
			"weight": 500
		},
		{
"sn":"f924c40c1b924d95878a702c4b3955s2",
			"num": 100,
			"price": 222,
			"saleNum": 2,
			"spec": "{\"电视音响效果\":\"测试222立体声\",\"测试电视屏幕尺寸\":\"测试22220英寸\",\"尺码\":\"测试22165\"}",
			"weight": 500
		}
	],
	"spu": {
"sn":"d08d32469a9b4d14869df30b53badasf",
		"brandId": 18374,
		"caption": "测试测试测试",
		"category3Id": 51,
		"id": 1146722015678321548,
		"name": "张宇洪手机"
	}
}
*/

    /**
     * 添加商品的操作  也可以是修改  判断传递过来的数据是否有spu的ID 有就是更新 没有就是添加
     *
     * @param goods 包含了spu和sku
     * @return
     */
    @ApiOperation(value = "添加商品")
    @PostMapping("/save")
    public Result saveGoods(@RequestBody Goods goods) {
        spuService.saveGoods(goods);
        return new Result(true, StatusCode.OK, "保存成功");
    }

    /**
     * 根据spu的ID 获取商品组合对象数据(点击编辑的时候回显数据)
     * @param id
     * @return
     */
    @ApiOperation(value = "编辑商品数据回显")
    @GetMapping("/goods/{id}")
    public Result<Goods> findGoodsById(@PathVariable(name = "id") Long id) {
        Goods goods = spuService.findGoodsById(id);
        return new Result(true, StatusCode.OK, "查询成功", goods);
    }

    @ApiOperation(value = "商品审核通过")
    @PutMapping("/audit/{id}")
    public Result audit(@PathVariable Long id) {
        spuService.audit(id);
        return new Result(true, StatusCode.OK, "审核成功");
    }

    @ApiOperation(value = "下架商品")
    @PutMapping("/pull/{id}")
    public Result pull(@PathVariable Long id) {
        spuService.pull(id);
        return new Result(true, StatusCode.OK, "下架成功");
    }

    @ApiOperation(value = "商品上架")
    @PutMapping("/put/{id}")
    public Result put(@PathVariable Long id) {
        spuService.put(id);
        return new Result(true, StatusCode.OK, "上架成功");
    }

    @ApiOperation(value = "批量上架")
    @PutMapping("/put/many")
    public Result putMany(@RequestBody Long[] ids) {
        int count = spuService.putMany(ids);
        return new Result(true, StatusCode.OK, "上架" + count + "个商品");
    }

    @ApiOperation(value = "批量上架")
    @PutMapping("/pull/many")
    public Result pullMany(@RequestBody Long[] ids) {
        int count = spuService.pullMany(ids);
        return new Result(true, StatusCode.OK, "上架" + count + "个商品");
    }

    /**
     * 逻辑删除
     * @param id
     * @return
     */
    @ApiOperation(value = "逻辑删除商品(非真正删除),必须先下架")
    @DeleteMapping("/logic/delete/{id}")
    public Result logicDelete(@PathVariable Long id){
        spuService.logicDelete(id);
        return new Result(true,StatusCode.OK,"逻辑删除成功！");
    }
    /**
     * 恢复数据
     * @param id
     * @return
     */
    @ApiOperation(value = "恢复删除的商品")
    @PutMapping("/restore/{id}")
    public Result restore(@PathVariable Long id){
        spuService.restore(id);
        return new Result(true,StatusCode.OK,"数据恢复成功！");
    }

    @ApiOperation(value = "真正删除商品,必须先逻辑删除")
    @DeleteMapping("/really/delete/{id}")
    public Result reallyDelete(@PathVariable Long id){
        spuService.reallyDelete(id);
        return new Result(true,StatusCode.OK,"逻辑删除成功！");
    }

}
