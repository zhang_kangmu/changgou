package com.changgou.order.service;

import com.changgou.order.pojo.OrderItem;

import java.util.List;

/**
 * Created by zhangyuhong
 * Date:2020/5/18
 */
//购物车
public interface CartService {
    /***
     * add的时候就就增加到缓存中了
     * 查询用户的购物车数据
     * @param username
     * @return
     */
    List<OrderItem> list(String username);

    void add(Integer num, Long id,String username);
}
