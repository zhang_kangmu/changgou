package com.changgou.order.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.order.config.TokenDecode;
import com.changgou.order.pojo.OrderItem;
import com.changgou.order.service.CartService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by zhangyuhong
 * Date:2020/5/18
 */
@Api(value = "购物车")
@RestController
@RequestMapping("/cart")
@CrossOrigin
public class CartController {
    @Autowired
    CartService cartService;

    @Autowired
    TokenDecode tokenDecode;
    /***
     * 加入购物车
     * @param num:购买商品数量
     * @param id：购买sku ID
     * @return
     */
    @GetMapping(value = "/add")
    public Result add(Integer num,Long id){
        String username = tokenDecode.getUsername();
        if (StringUtils.isEmpty(username)){
            return new Result(true, StatusCode.ERROR,"用户名不存在");
        }
        cartService.add(num,id,username);
        return new Result(true, StatusCode.OK,"增加购物车成功！");
    }

    /***
     * 查询用户购物车列表
     * @return
     */
    @GetMapping(value = "/list")
    public Result list(){
        //用户名
        String username = tokenDecode.getUsername();
        List<OrderItem> orderItems = cartService.list(username);
        return new Result(true, StatusCode.OK,"购物车列表查询成功！",orderItems);
    }
}
