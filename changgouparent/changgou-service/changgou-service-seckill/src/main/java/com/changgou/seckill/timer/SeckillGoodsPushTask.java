package com.changgou.seckill.timer;

import com.changgou.entity.DateUtil;
import com.changgou.entity.SystemConstants;
import com.changgou.seckill.dao.SeckillGoodsMapper;
import com.changgou.seckill.pojo.SeckillGoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhangyuhong
 * Date:2020/5/22
 */
@Component
public class SeckillGoodsPushTask {
    @Autowired
    private SeckillGoodsMapper seckillGoodsMapper;

    @Autowired
    private RedisTemplate redisTemplate;  //用于把订单存到redis中
    /**
     * 每5秒执行一次  "0/60 * * * * ? "  10秒执行一次
     * "0 0 0/1 * * ? * "  --这个会报错  去掉最后一个*号就可以了
     */
    @Scheduled(cron = "0/10 * * * * ?")  //一小时执行一次
    public void loadGoodsPushRedis(){
        System.out.println("定时器执行了...");
        //1.循环遍历以当前时间为基准的5个时间段     10:00 当前时间  12:00 14:00 16:00 18:00
        List<Date> dateMenus = DateUtil.getDateMenus();
        for (Date dateMenu : dateMenus) {
            //2.查询符合条件的商品的数据
            Example example = new Example(SeckillGoods.class);
            Example.Criteria criteria = example.createCriteria();
            // 1)商品必须审核通过  status=1
            criteria.andEqualTo("status","1");
            // 2)库存>0
            criteria.andGreaterThan("stockCount",0);
            // 3)开始时间<=活动开始时间
            criteria.andGreaterThanOrEqualTo("startTime",dateMenu);
            // 4)活动结束时间<开始时间+2小时
            criteria.andLessThan("endTime", DateUtil.addDateHour(dateMenu, 2));
            // 5)排除之前已经加载到Redis缓存中的商品数据
            String time = DateUtil.data2str(dateMenu, DateUtil.PATTERN_YYYYMMDDHH);
            Set keys = redisTemplate.boundHashOps(SystemConstants.SEC_KILL_GOODS_PREFIX+ time).keys();  //从redis里获取出这个秒杀商品key
            if (keys != null && keys.size()>0) {
                criteria.andNotIn("id", keys);
            }
            //查出对应的秒杀商品,在秒杀数据库里changgou_seckill
            List<SeckillGoods> seckillGoods = seckillGoodsMapper.selectByExample(example);
            if (seckillGoods != null) {
                for (SeckillGoods seckillGood : seckillGoods) {
                    //3.将其压入到redis中  SeckillGoods_2020052210为key  里面的map---id为商品id  值为内容
                    redisTemplate.boundHashOps(SystemConstants.SEC_KILL_GOODS_PREFIX + time).put(seckillGood.getId(), seckillGood);
                }
            }
            //给某一个key设置一个过期时间  2个小时

            //第一个参数  设置过期的key
            //第二个参数  设置过期的时间
            //第三个参数  过期时间的单位
            redisTemplate.expire(SystemConstants.SEC_KILL_GOODS_PREFIX + time,2, TimeUnit.HOURS);  //设置订单超时
        }
    }
}
