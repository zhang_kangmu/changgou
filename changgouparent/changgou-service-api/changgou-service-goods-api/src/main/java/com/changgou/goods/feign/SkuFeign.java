package com.changgou.goods.feign;

import com.changgou.entity.Result;
import com.changgou.goods.pojo.Sku;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by zhangyuhong
 * Date:2020/5/12
 */
@FeignClient(name="goods")
@RequestMapping(value = "/sku")
public interface SkuFeign {

    /***
     * 根据审核状态查询Sku
     * @param status
     * @return
     */
    @GetMapping("/status/{status}")  //restfuk风格一般要用PathVariable代表占位符
    Result<List<Sku>> findByStatus(@PathVariable(name="status") String status);

    /**
     * 根据条件搜索
     * @param sku
     * @return
     */
    @PostMapping(value = "/search" )
    public Result<List<Sku>> findList(@RequestBody(required = false) Sku sku);

    @GetMapping("/{id}")
    public Result<Sku> findById(@PathVariable(name = "id") Long id);

    @GetMapping("/decCount")
    public Result decCount(@RequestParam(name = "id") Long id, @RequestParam(name = "num") Integer num) ;
}
