package com.changgou.search.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by zhangyuhong
 * Date:2020/5/13
 */
@FeignClient(value = "search")
@RequestMapping("/search")
public interface SkuFeign {
    @GetMapping
    Map search(@RequestParam(required = false) Map searchMap);
}
