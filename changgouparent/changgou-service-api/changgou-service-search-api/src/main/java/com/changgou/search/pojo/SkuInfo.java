package com.changgou.search.pojo;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Created by zhangyuhong
 * Date:2020/5/12
 */
@Document(indexName = "skuinfo",type = "docs")
@Data
public class SkuInfo implements Serializable {
    //商品id，同时也是商品编号
    @Id   //注意这个id不要导错了
    private Long id;
    //SKU名称
    @Field(type = FieldType.Text, analyzer = "ik_smart")
    private String name;


    //商品价格，单位为：元
    @Field(type = FieldType.Double)
    private Long price;

    //库存数量
    private Integer num;

    //商品图片
    private String image;

    //商品状态，1-正常，2-下架，3-删除
    private String status;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

    //是否默认
    private String isDefault;

    //SPUID
    private Long spuId;

    //类目ID
    private Long categoryId;

    //类目名称
    @Field(type = FieldType.Keyword)  // 表示字符串 关键字类型 ，一定是不分词的,就是精准查询
    private String categoryName;

    //品牌名称
    @Field(type = FieldType.Keyword)
    private String brandName;

    //规格  还是跟数据库一样的,规格的字符串  比如:{"电视音响效果":"立体声","电视屏幕尺寸":"20英寸","尺码":"165"}
    private String spec;

    //规格参数,这个很重要,有很多种规格  比如:{"电视音响效果":"立体声","电视屏幕尺寸":"20英寸","尺码":"165"}全是key value
    //因为数据库里的都是一个长字符串,所以转到es中就部能这样了,要拆分成 Map<String,Object>,所以有了这个字段
    private Map<String,Object> specMap;
}
