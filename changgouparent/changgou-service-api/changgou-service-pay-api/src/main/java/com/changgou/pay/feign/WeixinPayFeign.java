package com.changgou.pay.feign;

import com.changgou.entity.Result;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by zhangyuhong
 * Date:2020/5/26
 */
@RequestMapping(value = "/weixin/pay")
public interface WeixinPayFeign {

    @RequestMapping("/pay/close")
    public Result closePay(Long orderId);
}
