package com.changgou;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by zhangyuhong
 * Date:2020/5/15
 */
public class ParseJwt {
    /***
     * 解析Jwt令牌数据
     */
    @Test
    public void testCreateJwt() throws InterruptedException {
        JwtBuilder builder = Jwts.builder()
                .setId("8988")     //设置唯一编号
                .setSubject("测试")   //设置主题  可以是JSON数据
                .setIssuedAt(new Date())   //设置签发日期
//                .setExpiration(new Date())  //设置过期时间
                .signWith(SignatureAlgorithm.HS256,"zhangyuhong"); //设置签名 使用HS256算法，并设置SecretKey(字符串)
        HashMap<String, Object> map = new HashMap<>();
        map.put("username", "张");
        map.put("age", 18);
        builder.addClaims(map);
        System.out.println( builder.compact() );

    }
    /***
     * 解析Jwt令牌数据
     */
    @Test
    public void testParseJwt(){
        String compactJwt="eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4OTg4Iiwic3ViIjoi5rWL6K-VIiwiaWF0IjoxNTg5ODA2OTUxLCJhZ2UiOjE4LCJ1c2VybmFtZSI6IuW8oCJ9.s3OgW917-21SHxFVRXm39zX_lX2ZqzN3M_n6T-FAxaQ";
        Claims claims = Jwts.parser().
                setSigningKey("zhangyuhong").
                parseClaimsJws(compactJwt).
                getBody();
        System.out.println(claims);
    }
}
