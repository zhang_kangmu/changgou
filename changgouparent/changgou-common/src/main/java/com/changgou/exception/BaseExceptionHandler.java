package com.changgou.exception;

/**
 * Created by zhangyuhong
 * Date:2020/5/6
 */

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理类
 */
@ControllerAdvice  //标识是一个异常处理类
public class BaseExceptionHandler {
    @ExceptionHandler(Exception.class)  //标识那些有requestMapper相关注解的controller出现异常走这里,普通的异常不走
    @ResponseBody
    public Result error(Exception e) {
        e.printStackTrace();    //如果不写这个,控制台中不显示堆栈的错误信息
        return new Result(false, StatusCode.ERROR, e.getMessage());
    }

    @ExceptionHandler(MyGoodsExecption.class)  //标识那些有requestMapper相关注解的controller出现异常走这里,普通的异常不走
    @ResponseBody
    public Result error(MyGoodsExecption e) {
        e.printStackTrace();    //如果不写这个,控制台中不显示堆栈的错误信息
        return new Result(false, StatusCode.ERROR, e.getMessage());
    }
}
