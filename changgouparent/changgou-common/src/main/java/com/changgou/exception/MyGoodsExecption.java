package com.changgou.exception;

import io.swagger.annotations.ApiModel;

/***
 * 描述
 * @author zhangyuhogn
 * @version 1.0
 * @date 2020/5/22
 */
@ApiModel(value = "秒杀商品 自定义异常")
public class MyGoodsExecption extends RuntimeException {
    /**
     * 接受状态码和消息
     * @param message
     */
    public MyGoodsExecption( String message) {
        super(message);
    }
}
