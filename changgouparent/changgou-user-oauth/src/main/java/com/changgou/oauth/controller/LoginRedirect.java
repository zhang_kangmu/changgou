package com.changgou.oauth.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by zhangyuhong
 * Date:2020/5/19
 */
@Controller  //这里不是restController,仅仅跳转而已
@RequestMapping(value = "/oauth")
//登录用的
public class LoginRedirect {
    /***
     * 跳转到登录页面
     * @return
     */
    @GetMapping(value = "/login")  //参数不是必须的
    public String login(@RequestParam(value = "FROM",required = false,defaultValue = "")String from, Model model){
        model.addAttribute("from", from);  //传递重定向的地址回去
        return "login";
    }
}
