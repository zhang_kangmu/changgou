package com.changgou.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * Created by zhangyuhong
 * Date:2020/5/16
 */
@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {
    //令牌头名字
    private static final String AUTHORIZE_TOKEN = "Authorization";
    private static final String LOGIN_URL = "http://localhost:9001/oauth/login";  //登录的网页,非授权的那个地址

    //这里处理权限校验的逻辑
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1.获取Request、Response对象
        ServerHttpRequest request = exchange.getRequest();
        //2.Response对象
        ServerHttpResponse response = exchange.getResponse();
        //3.获取当前的请求路径 判断是否属于要去登录路径，如果是 放行
        String path = request.getURI().getPath();
        //AuthController.java里的login里了
        //如果是登录、goods等开放的微服务[这里的goods部分开放],则直接放行,这里不做完整演示，完整演示需要设计一套权限系统
        if (path.startsWith("/api/user/login")){
            //如果是登录的请求,那就直接让他去登录就好
            return chain.filter(exchange);
        }
        //4.判断是否有token  如果没有 直接返回
        //4.1 先从请求参数中获取token
        String token = request.getQueryParams().getFirst(AUTHORIZE_TOKEN);
        //4.2 没有,再去从头中获取token
        if (StringUtils.isEmpty(token)) {
            token = request.getHeaders().getFirst(AUTHORIZE_TOKEN);
        }
        if (StringUtils.isEmpty(token)) {
            //4.3 再去cookie中获取token
            HttpCookie cookie = request.getCookies().getFirst(AUTHORIZE_TOKEN);
            if (cookie != null) {
                token = cookie.getValue();
            }
        }
        //  没有令牌
        if (StringUtils.isEmpty(token)) {
            //response.setStatusCode(HttpStatus.UNAUTHORIZED);  //未经授权
            //没有令牌  说明无权限 直接返登录页面  重定向到登录页面
            response.getHeaders().set("Location",LOGIN_URL+"?FROM="+request.getURI().toString());  //FROM后面的地址 表示从那个页面跳转过来登录的  登录完后准备跳转到的页面
            response.setStatusCode(HttpStatus.SEE_OTHER); //303重定向状态码
            return response.setComplete();  //完成了本地请求,报出异常,401, "Unauthorized"  该网页无法正常运作
        }
        //5.如果有token  校验令牌 校验不通过 返回
        try {

            //JwtUtil.parseJWT(token);  //能通过这个解析,符合令牌的标准  以前测试用的  这里就不需要(因为有了网关),而是将token放在头中  传出去,给其他的微服务用
            //将令牌获取到 放入头部中传给给下一个微服务  mutate():更改一个资源的状态
            request.mutate().header(AUTHORIZE_TOKEN,"bearer "+token);  //携带授权码,传递授权码
        } catch (Exception e) {
            e.printStackTrace();
            //说明校验失败，错误信息
            response.setStatusCode(HttpStatus.UNAUTHORIZED);

            return response.setComplete();
        }

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
